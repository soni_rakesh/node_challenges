

Assignment link 
https://github.com/reedsy/challenges/blob/master/node-fullstack.md

---------------------------------------------------------------------

Note : Please download and refer the word document "ReadMe.docx" for details.

---------------------------------------------------------------------


(1) API document for "3. Node.js REST API"
http://5exceptions.com/apidocs/
Note : Use any REST API client(POSTMAN etc) for testing 


(2) AngularJS
http://5exceptions.com:3000/bookhome


(3) Bonus question
http://5exceptions.com/bonus/


---------------------------------------------------------------------

Source code  : GIT Repository location
https://bitbucket.org/soni_rakesh/node_challenges/src/master/

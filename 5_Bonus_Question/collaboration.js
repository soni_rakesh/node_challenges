/*

When multiple users are collaborating on a document, collisions in their edits inevitably occur.
Implement a module that can handle basic text update operations, and combine two colliding edits
into a single operation.

An operation is described as an array of any combination of three types of edits:

    { move: number } to advance the caret
    { insert: string } to insert the string at caret
    { delete: number } to delete a number of chars from the caret onwards

Implement the following methods:

    Operation.prototype.combine(operation) Updates the operation by combining it with another colliding operation
    Operation.combine(op1, op2) Static method that returns a new operation by combining the arguments without mutating them
    Operation.prototype.apply(string) Applies the operation to the provided argument

Please refer the link for more detail
https://github.com/reedsy/challenges/blob/master/node-fullstack.md
    
*/
var Operation = /** @class */ (function () {
    // -----------------------------------------------------------------
    // constructor 
    function Operation(jsonArr) {
        this.operationJsonArr = null;
        // Logger.log(jsonArr);
        this.operationJsonArr = jsonArr;
    }
    // -----------------------------------------------------------------
    // method to applies the operation to the provided argument
    Operation.prototype.apply = function (strInput) {
        var strTemp = strInput;
        var cursorPos = 0;
        // Logger.log(" length= " + this.operationJsonArr.length);
        Logger.log(this.operationJsonArr);
        for (var i = 0; i < this.operationJsonArr.length; i++) {
            if (this.operationJsonArr[i].move) {
                cursorPos = cursorPos + this.operationJsonArr[i].move;
            }
            if (this.operationJsonArr[i].insert) {
                var val = this.operationJsonArr[i].insert;
                var strPart1 = strTemp.substr(0, cursorPos);
                var strPart2 = strTemp.substr(cursorPos);
                strTemp = strPart1 + val + strPart2;
                cursorPos = cursorPos + val.length;
            }
            if (this.operationJsonArr[i]["delete"]) {
                var val = this.operationJsonArr[i]["delete"];
                var strPart1 = strTemp.substr(0, cursorPos);
                var strPart2 = strTemp.substr(cursorPos + val);
                strTemp = strPart1 + strPart2;
            }
        }
        return strTemp;
    };
    // -------------------------------------------------------------------------------------------
    // Static method that returns a new operation by combining the arguments without mutating them
    Operation.combine = function (op1, op2) {
        var resultOp = null;
        var strResult = "";
        var sumMovePosOper1 = 0;
        var sumMovePosOper2 = 0;
        var lastPosOfCusror = 0;
        // let idxOp1 = 0;
        var idxOp2 = 0; // index of operation 2
        // operation 1
        for (var i = 0; i < op1.operationJsonArr.length; i++) {
            if (op1.operationJsonArr[i].move) {
                // temp pos = sum till last move + current move 
                var tempPos1 = sumMovePosOper1 + op1.operationJsonArr[i].move;
                var tempPos2 = 0;
                // operation 2
                for (var j = idxOp2; j < op2.operationJsonArr.length; j++) {
                    if (op2.operationJsonArr[j].move) {
                        // temp pos = sum till last move + current move 
                        tempPos2 = sumMovePosOper2 + op2.operationJsonArr[j].move;
                        // insert move operation 
                        if (tempPos2 < tempPos1) {
                            sumMovePosOper2 = tempPos2;
                            if (strResult.length > 1) {
                                strResult += ', {' + '"move":' + (sumMovePosOper2 - lastPosOfCusror) + '}';
                            }
                            else {
                                strResult += '{' + '"move":' + (sumMovePosOper2 - lastPosOfCusror) + '}';
                            }
                            idxOp2 = idxOp2 + 1;
                            lastPosOfCusror = sumMovePosOper2;
                        }
                        else {
                            break;
                        }
                    }
                    if (tempPos2 < tempPos1) {
                        // insert operation   
                        if (op2.operationJsonArr[j].insert) {
                            if (strResult.length > 1) {
                                strResult += ', {' + '"insert": "' + op2.operationJsonArr[j].insert + '"}';
                            }
                            else {
                                strResult += '{' + '"insert": "' + op2.operationJsonArr[j].insert + '"}';
                            }
                            idxOp2 = idxOp2 + 1;
                        }
                        // delete operation   
                        if (op2.operationJsonArr[j]["delete"]) {
                            if (strResult.length > 1) {
                                strResult += ', {' + '"delete": "' + op2.operationJsonArr[j]["delete"] + '"}';
                            }
                            else {
                                strResult += '{' + '"delete": "' + op2.operationJsonArr[j]["delete"] + '"}';
                            }
                            idxOp2 = idxOp2 + 1;
                        }
                    }
                    else {
                        break;
                    }
                }
                sumMovePosOper1 = tempPos1;
                if (strResult.length > 1) {
                    strResult += ', { ' + '"move":' + (sumMovePosOper1 - lastPosOfCusror) + '}';
                }
                else {
                    strResult += '{ ' + '"move":' + (sumMovePosOper1 - lastPosOfCusror) + '}';
                }
                lastPosOfCusror = sumMovePosOper1;
            }
            // insert operation   
            if (op1.operationJsonArr[i].insert) {
                if (strResult.length > 1) {
                    strResult += ', {' + '"insert": "' + op1.operationJsonArr[i].insert + '"}';
                }
                else {
                    strResult += '{' + '"insert": "' + op1.operationJsonArr[i].insert + '"}';
                }
            }
            // delete operation   
            if (op1.operationJsonArr[i]["delete"]) {
                if (strResult.length > 1) {
                    strResult += ', {' + '"delete": "' + op1.operationJsonArr[i]["delete"] + '"}';
                }
                else {
                    strResult += '{' + '"delete": "' + op1.operationJsonArr[i]["delete"] + '"}';
                }
            }
        }
        // operation 2 : remaing items (which was not merged in above loop)
        for (var j = idxOp2; j < op2.operationJsonArr.length; j++) {
            if (op2.operationJsonArr[j].move) {
                var tempPos2 = sumMovePosOper2 + op2.operationJsonArr[j].move;
                sumMovePosOper2 = tempPos2;
                if (strResult.length > 1) {
                    strResult += ', { ' + '"move":' + (sumMovePosOper2 - lastPosOfCusror) + '}';
                }
                else {
                    strResult += '{ ' + '"move":' + (sumMovePosOper2 - lastPosOfCusror) + '}';
                }
                lastPosOfCusror = sumMovePosOper2;
            }
            // insert operation   
            if (op2.operationJsonArr[j].insert) {
                if (strResult.length > 1) {
                    strResult += ', {' + '"insert": "' + op2.operationJsonArr[j].insert + '"}';
                }
                else {
                    strResult += '{' + '"insert": "' + op2.operationJsonArr[j].insert + '"}';
                }
            }
            // delete operation   
            if (op2.operationJsonArr[j]["delete"]) {
                if (strResult.length > 1) {
                    strResult += ', {' + '"delete": "' + op2.operationJsonArr[j]["delete"] + '"}';
                }
                else {
                    strResult += '{' + '"delete": "' + op2.operationJsonArr[j]["delete"] + '"}';
                }
            }
        }
        // Logger.log("[" + strResult + "]");
        var jsonObj = JSON.parse("[" + strResult + "]");
        resultOp = new Operation(jsonObj);
        return resultOp;
    };
    // -------------------------------------------------------------------------------------------
    // Applies the operation to the provided argument
    Operation.prototype.combine = function (op2) {
        // let op1:Operation = this;
        var resultOp = null;
        var strResult = "";
        var sumMovePosOper1 = 0;
        var sumMovePosOper2 = 0;
        var lastPosOfCusror = 0;
        // let idxOp1 = 0;
        var idxOp2 = 0; // index of operation 2
        // operation 1
        for (var i = 0; i < this.operationJsonArr.length; i++) {
            if (this.operationJsonArr[i].move) {
                // temp pos = sum till last move + current move 
                var tempPos1 = sumMovePosOper1 + this.operationJsonArr[i].move;
                var tempPos2 = 0;
                // operation 2
                for (var j = idxOp2; j < op2.operationJsonArr.length; j++) {
                    if (op2.operationJsonArr[j].move) {
                        // temp pos = sum till last move + current move 
                        tempPos2 = sumMovePosOper2 + op2.operationJsonArr[j].move;
                        // insert move operation 
                        if (tempPos2 < tempPos1) {
                            sumMovePosOper2 = tempPos2;
                            if (strResult.length > 1) {
                                strResult += ', {' + '"move":' + (sumMovePosOper2 - lastPosOfCusror) + '}';
                            }
                            else {
                                strResult += '{' + '"move":' + (sumMovePosOper2 - lastPosOfCusror) + '}';
                            }
                            idxOp2 = idxOp2 + 1;
                            lastPosOfCusror = sumMovePosOper2;
                        }
                        else {
                            break;
                        }
                    }
                    if (tempPos2 < tempPos1) {
                        // insert operation   
                        if (op2.operationJsonArr[j].insert) {
                            if (strResult.length > 1) {
                                strResult += ', {' + '"insert": "' + op2.operationJsonArr[j].insert + '"}';
                            }
                            else {
                                strResult += '{' + '"insert": "' + op2.operationJsonArr[j].insert + '"}';
                            }
                            idxOp2 = idxOp2 + 1;
                        }
                        // delete operation   
                        if (op2.operationJsonArr[j]["delete"]) {
                            if (strResult.length > 1) {
                                strResult += ', {' + '"delete": "' + op2.operationJsonArr[j]["delete"] + '"}';
                            }
                            else {
                                strResult += '{' + '"delete": "' + op2.operationJsonArr[j]["delete"] + '"}';
                            }
                            idxOp2 = idxOp2 + 1;
                        }
                    }
                    else {
                        break;
                    }
                }
                sumMovePosOper1 = tempPos1;
                if (strResult.length > 1) {
                    strResult += ', { ' + '"move":' + (sumMovePosOper1 - lastPosOfCusror) + '}';
                }
                else {
                    strResult += '{ ' + '"move":' + (sumMovePosOper1 - lastPosOfCusror) + '}';
                }
                lastPosOfCusror = sumMovePosOper1;
            }
            // insert operation   
            if (this.operationJsonArr[i].insert) {
                if (strResult.length > 1) {
                    strResult += ', {' + '"insert": "' + this.operationJsonArr[i].insert + '"}';
                }
                else {
                    strResult += '{' + '"insert": "' + this.operationJsonArr[i].insert + '"}';
                }
            }
            // delete operation   
            if (this.operationJsonArr[i]["delete"]) {
                if (strResult.length > 1) {
                    strResult += ', {' + '"delete": "' + this.operationJsonArr[i]["delete"] + '"}';
                }
                else {
                    strResult += '{' + '"delete": "' + this.operationJsonArr[i]["delete"] + '"}';
                }
            }
        }
        // operation 2 : remaing items (which was not merged in above loop)
        for (var j = idxOp2; j < op2.operationJsonArr.length; j++) {
            if (op2.operationJsonArr[j].move) {
                var tempPos2 = sumMovePosOper2 + op2.operationJsonArr[j].move;
                sumMovePosOper2 = tempPos2;
                if (strResult.length > 1) {
                    strResult += ', { ' + '"move":' + (sumMovePosOper2 - lastPosOfCusror) + '}';
                }
                else {
                    strResult += '{ ' + '"move":' + (sumMovePosOper2 - lastPosOfCusror) + '}';
                }
                lastPosOfCusror = sumMovePosOper2;
            }
            // insert operation   
            if (op2.operationJsonArr[j].insert) {
                if (strResult.length > 1) {
                    strResult += ', {' + '"insert": "' + op2.operationJsonArr[j].insert + '"}';
                }
                else {
                    strResult += '{' + '"insert": "' + op2.operationJsonArr[j].insert + '"}';
                }
            }
            // delete operation   
            if (op2.operationJsonArr[j]["delete"]) {
                if (strResult.length > 1) {
                    strResult += ', {' + '"delete": "' + op2.operationJsonArr[j]["delete"] + '"}';
                }
                else {
                    strResult += '{' + '"delete": "' + op2.operationJsonArr[j]["delete"] + '"}';
                }
            }
        }
        // Logger.log("[" + strResult + "]");
        var jsonObj = JSON.parse("[" + strResult + "]");
        this.operationJsonArr = jsonObj;
        // return resultOp;
    };
    return Operation;
}());
// class end 
var Logger = /** @class */ (function () {
    function Logger() {
    }
    Logger.log = function (msg) {
        if (Logger.print_console) {
            console.log(msg);
        }
        if (Logger.print_page) {
            var str = JSON.stringify(msg, null, 2); // spacing level = 2
            document.write("<BR>" + str);
        }
    };
    Logger.print_console = true;
    Logger.print_page = true;
    return Logger;
}());
// -------------------------------------------------------------------------------------------
// Testing 
var s = "abcdefg";
var op1 = new Operation([{ move: 1 }, { insert: "FOO" }]);
var op2 = new Operation([{ move: 3 }, { insert: "BAR" }]);
Logger.log("Operation 1");
Logger.log(op1);
Logger.log("Operation 2");
Logger.log(op2);
Logger.log("------------------------------------------------");
// apply 
var result1 = op1.apply(s); // => "aFOObcdefg"
Logger.log("Result of apply() operation =" + result1);
var result2 = op2.apply(s); // => "abcBARdefg"
Logger.log("Result of apply() operation =" + result2);
// Testing with given use case in assignment 
var combined1 = Operation.combine(op1, op2); // => [{ move: 1 }, { insert: 'FOO' }, { move: 2}, { insert: 'BAR' } ]
Logger.log("------------ combined Result ------------- ");
Logger.log(combined1);
var combineResult1 = combined1.apply(s); // => "aFOObcBARdefg"
Logger.log("Result of apply operation on combine result =" + combineResult1);
Logger.log("<BR><BR> =========================== Test 2===========================================");
// Testing with multiple operation 
var op3 = new Operation([{ move: 3 }, { insert: "AAA" }, { move: 5 }, { insert: "BBB" }, { move: 2 }, { insert: "CCC" }]);
var op4 = new Operation([{ move: 1 }, { insert: "PPP" }, { move: 3 }, { insert: "QQQ" }, { move: 2 }, { insert: "RRR" }]);
Logger.log("Operation 3");
Logger.log(op3);
Logger.log("Operation 4");
Logger.log(op4);
Logger.log("------------------------------------------------");
var combinedTest2 = Operation.combine(op3, op4);
Logger.log("------------ combined method Result ------------- ");
Logger.log(combinedTest2);
var combineResult2 = combinedTest2.apply(s);
Logger.log("Result of apply method on combine result =" + combineResult2);
// Testing with prototype method 
Logger.log("=========================== Test  with prototype method ============================");
op1.combine(op2); // => [{ move: 1 }, { insert: 'FOO' }, { move: 2}, { insert: 'BAR' } ]
Logger.log("------------ combined with prototype method ------------- ");
Logger.log(op1);
var combineResult3 = op1.apply(s); // => "aFOObcBARdefg"
Logger.log("Result of combine operation 1=" + combineResult3);
Logger.log("<BR><BR>=========================== Test 3 with delete operation =================================");
var s2 = "abcdefghijklmnop";
var op5 = new Operation([{ move: 1 }, { insert: "FOO" }, { move: 3 }, { "delete": 2 }]);
var op6 = new Operation([{ move: 3 }, { "delete": 4 }, { insert: "BAR" }]);
Logger.log("Operation 5");
Logger.log(op5);
Logger.log("Operation 6");
Logger.log(op6);
Logger.log("------------------------------------------------");
// apply 
var result1 = op5.apply(s2); // => "aFOObcdefg"
Logger.log("Result of apply() operation =" + result1);
var result2 = op6.apply(s2); // => "abcBARdefg"
Logger.log("Result of apply() operation =" + result2);
// Testing with given use case in assignment 
var combined5 = Operation.combine(op5, op6);
Logger.log("------------ combined Result ------------- ");
Logger.log(combined5);
var combineResult1 = combined1.apply(s2); // => "aFOObcBARdefg"
Logger.log("Result of apply operation on combine result =" + combineResult1);
